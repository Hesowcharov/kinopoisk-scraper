package utils

/**
  * Created by HesowcharovU on 12.12.2016.
  */
import com.github.nscala_time.time.Imports._

object IntervalIterators {
  implicit class ImplicitIterator(val interval: Interval) extends AnyVal {
    def iterateBy(step: Period): Iterator[DateTime] = Iterator.iterate(interval.start) { _ + step }
      .takeWhile(_.isBefore(interval.end))

    def iterateBy[A](step: Period, transform: DateTime => A): Iterator[A] = iterateBy(step).map(transform)

    def iterateByDay: Iterator[LocalDate] = iterateBy(1.day, { _.toLocalDate })

    def iterateByHour: Iterator[DateTime] = iterateBy(1.hour)
  }
}
