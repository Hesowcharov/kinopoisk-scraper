package utils

import scala.util.Random

/**
  * Created by HesowcharovU on 02.12.2016.
  */

import com.github.nscala_time.time.Imports._

object Utils {
  val dbName = "KrasnodarCinemasDB"
  val baseCinemaPrices = Map(
    "Болгария" -> 150,
    "Европа" -> 120,
    "Киномакс-Галерея Краснодар" -> 200,
    "Киноцентр на Красной площади" -> 200,
    "Кубанькино" -> 120,
    "Семь Звезд" -> 200,
    "Сити de Luxe" -> 180,
    "Формула кино Oz" -> 200
  )
  val dayFees = Map(
    "Понедельник" -> -10,
    "Вторник" -> -10,
    "Среда" -> 20,
    "Четверг" -> 50,
    "Пятница" -> 100,
    "Суббота" -> 130,
    "Воскресенье" -> 100
  )
  val rand = new Random(System.currentTimeMillis())

  def getInsertBaseHeader(table: String) = s"use $dbName\n INSERT INTO $table"

  def getInsertHeader(table: String) =
    s"${getInsertBaseHeader(table)} VALUES \n"

  def getInsertHeader(table: String, columns: Seq[String]): String = {
    val cols = columns.mkString("(", ", ", ")")
    s"${getInsertBaseHeader(table)} $cols VALUES \n"
  }

  def selectScalarValues(table: String, cols: String*)(whereCond: (String, String)*): String = {
    val columns = cols.mkString("(", ", ", ")")
    val whereConstraints = whereCond.map { case (c, v) => s"$c = ${wrapString(v)}"}
      .mkString(" AND ")
    val baseSelect = s"SELECT TOP 1 $columns FROM $table"
    if (whereCond.length == 0)
      baseSelect
    else
      s"$baseSelect WHERE $whereConstraints"

  }

  def getInsertHeaderViaSelect(table: String) =
    s"${getInsertBaseHeader(table)} SELECT TOP 1 \n"

  def writeToFile(script: String, pathToFile: String): Unit = {
    import java.io._
    val pw = new PrintWriter(new File(pathToFile))
    pw.write(script)
    pw.close
  }

  def readFromFile(path: String): List[String] =
    io.Source.fromFile(path).getLines().toList

  def wrapString(str: String): String = s"N'${str.replace('\'', '`')}'"
  def wrapDate(str: String): String = s"'$str'"
  def convertToString(date: DateTime) = date toString "yyyy-MM-dd"

  def iterativelyScrape[L, D](scraper: (L) => D,
                              partHandler: (List[D], Int) => Unit,
                              links: List[L],
                              partitionSize: Int)(startFrom: Int = 1): Unit = {

    def partScrape(part: List[L]): List[D] = part map scraper

    def scrape(parts: List[List[L]], startFrom: Int): Unit = {
      parts
        .zipWithIndex
        .map { case (e, i) => (e, i+1) }
        .drop(startFrom - 1)
        .foreach { case (ls, i) =>
          val partResult = partScrape(ls)
          partHandler(partResult, i)
        }
    }

    val partedLinks = links.grouped(partitionSize).toList
    scrape(partedLinks, startFrom)
  }

  def getRandomPositionsBetween(leftDecimalBorder: Int, rightDecimalBorder: Int): Int = {
    val dec = leftDecimalBorder + ((rightDecimalBorder - leftDecimalBorder) * rand.nextDouble()).toInt + 1
    dec * 10
  }


}