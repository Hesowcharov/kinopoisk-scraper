package data

/**
  * Created by HesowcharovU on 12.12.2016.
  */
case class Show (cinema: String, movie: Movie, date: String, positions: Int, price: Int)
