package data

/**
  * Created by HesowcharovU on 07.12.2016.
  */
case class FilmUnit(firstName: String,
                    lastName: Option[String],
                    birthdate: Option[String],
                    country: Option[String])