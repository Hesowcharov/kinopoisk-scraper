package data

/**
  * Created by HesowcharovU on 07.12.2016.
  */
case class Movie(name: String,
                 originalName: String,
                 slogan: Option[String],
                 premiereDate: String,
                 countries: Seq[String],
                 genres: Seq[String],
                 budget: Option[String],
                 requiredAge: Option[Int],
                 length: Option[Int],
                 description: Option[String])