package apps

import net.ruippeixotog.scalascraper.scraper.ContentExtractors.elementList
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.scraper.ContentExtractors.text
import utils.Utils._
import data.Award


/**
  * Created by HesowcharovU on 22.12.2016.
  */
object AwardScraper extends App with ScrapStarter[List[Award]] with ScraperOf[List[Award]] {
  val links = List("https://www.kinopoisk.ru/awards/")

  val partitionSize = args(0) toInt

  val startPartition = args(1) toInt

  val pathToWrite = args(2)

  override def elementScrap(link: String): List[Award] = {
    val browser = JsoupBrowser()
    val doc = browser.get(link)
    val awards = (doc.body >> elementList("td.news"))
      .map(_ >?> text("a b"))
      .filter(_.isDefined)
      .map { opt => Award(opt.get) }
    awards
  }

  override def partitionHandler(partition: List[List[Award]], number: Int): Unit = {
    val table = "FilmAward"
    val script = partition
      .flatten
      .map { case Award(name) =>  name.head.toUpper + name.tail }
      .map { wrapString _ }
      .mkString(getInsertHeader(table), ", \n" , "")
    writeToFile(script, s"$pathToWrite$table.sql")
  }

  start()
}
