package apps

import net.ruippeixotog.scalascraper.scraper.ContentExtractors.elementList
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL._
/**
  * Created by HesowcharovU on 22.12.2016.
  */
object MoviePremiereLinksScraper extends App {
  def getPath(year: Int, month: Int): String =
    s"D:/scrap_data/${year}_$month.htm"

  def collectMovieLinks(premLinks: List[String], movieLinks: List[String]): List[String] = {
    premLinks match {
      case path :: tail =>
        val doc = JsoupBrowser().parseFile(path).body
        val newMovieLinks = (doc >> elementList(".premier_item span[itemprop=\"name\"] a"))
          .map { _.attr("href") }
        collectMovieLinks(tail, movieLinks ::: newMovieLinks)
      case _ => movieLinks
    }
  }

  val premLinks = (for {year <- 2016 to 2017; month <- 1 to 2}
    yield if (year == 2016)
      getPath(year, 13 - month)
    else
      getPath(year, month)).toList

  val movieLinks = collectMovieLinks(premLinks, List())
    .distinct
    .mkString("\n")
  import java.io._
  val pw = new PrintWriter(new File("D:/sql_scripts/links/hot_movies.txt"))
  pw.write(movieLinks)
  pw.close
}
