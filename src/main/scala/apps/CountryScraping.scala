package apps

import net.ruippeixotog.scalascraper.scraper.ContentExtractors.texts
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL._
import utils.Utils._


/**
  * Created by HesowcharovU on 22.12.2016.
  */
object CountryScraping extends App {
  val table = "Country"
  val doc = JsoupBrowser().get("https://www.kinopoisk.ru/s/")
  val countryOptions = doc.body >> texts("select#country option")
  val uniqueCountries = countryOptions.filter(opt => opt.trim != "-" && !opt.isEmpty)
    .toSet.toList
  val script = uniqueCountries.map {c => s"(N'$c')"}
    .mkString(getInsertHeader(table), ", \n", "")
  writeToFile(script, table)
}
