package apps

import utils.Utils._

object MergeFiles extends App {

  val dir = args(0)
  val filename = args(1)
  val upTo = args(2) toInt

  val files = (1 to upTo).map {n => s"$dir${filename}_$n.sql"}.toList
  val mergedFiles = files.flatMap(readFromFile).mkString("\n")
  writeToFile(mergedFiles, s"$dir$filename.sql")
}