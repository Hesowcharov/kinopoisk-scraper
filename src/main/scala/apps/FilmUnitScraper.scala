package apps

import net.ruippeixotog.scalascraper.scraper.ContentExtractors.{attr, elementList, text, texts}
import utils.Utils._
import net.ruippeixotog.scalascraper.browser._
import net.ruippeixotog.scalascraper.dsl.DSL._

/**
  * Created by HesowcharovU on 22.12.2016.
  */
object FilmUnitScraper extends App {
  import data.FilmUnit

  val actorLinks = readFromFile(args(0))
  val partitionSize = args(1) toInt
  val startFrom = args(2) toInt
  val pathToWrite = args(3)
  val table = "FilmUnit"
  val cols = List("FirstName", "LastName", "Birthdate", "Birthplace")
  val table2 = "Country"

  def scrapActorData(link: String): FilmUnit = {
    println(s"$link is started")
    val docBody = JsoupBrowser().get(link).body
    println("\tname parsing...")
    val names = (docBody >> text("#headerPeople h1")).split(' ')
    val firstName = names(0)
    val lastName = if (names.size == 1) {
      None
    } else {
      Some(names.drop(1).mkString(" "))
    }
    println("\tbirthdate parsing...")
    val optBirthdate = (docBody >?> attr("birthdate")("td.birth")).map( b => s"'$b'" )
    println("\tcountry parsing...")
    val optCountry = ((docBody >> elementList("table.info td"))
      .dropWhile { _.text != "место рождения" }
      .drop(1)
      .head >?> texts("a"))
      .flatMap(_.lastOption)
      .map(wrapString)
    println(s"$link was handled\n")
    FilmUnit(firstName, lastName, optBirthdate, optCountry)
  }

  def partHandler(filmUnitData: List[FilmUnit], number: Int): Unit = {
    val filmUnitScript = getInsertHeader(table, cols) +
      filmUnitData.map { case FilmUnit(fn, ln, birthdate, birthplace) =>
        val cId = birthplace match {
          case None => "NULL"
          case Some(x) => s"(SELECT TOP 1 ID FROM $table2 WHERE Name = ${wrapString(x)})"
        }
        s"(${wrapString(fn)}, ${ln.map(wrapString).getOrElse("NULL")}, ${birthdate.map(wrapDate).getOrElse("NULL")}, $cId)"
      }.mkString(",\n")
    writeToFile(filmUnitScript, s"$pathToWrite${table}_$number.sql")
    println(s"Partition #$number has been done.")
  }

  iterativelyScrape(scrapActorData, partHandler, actorLinks, partitionSize)(startFrom)
}
