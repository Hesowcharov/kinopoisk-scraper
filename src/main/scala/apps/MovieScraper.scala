package apps

import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.model.Element
import net.ruippeixotog.scalascraper.scraper.ContentExtractors._
import net.ruippeixotog.scalascraper.dsl.DSL._
import utils.Utils._

/**
  * Created by HesowcharovU on 22.12.2016.
  */
object MovieScraper extends App {

  import data.Movie

  val pathToLinks = args(0)
  val partitionSize = args(1) toInt
  val startFrom = args(2) toInt
  val pathToWrite = args(3)
  val movieTable = "Movie"
  val movieCols = List("Name", "OriginalName", "Slogan", "PremiereDate", "Budget", "RequiredAge", "Length", "Description")
  val genreTable = "Movie_Genre"
  val countryTable = "Movie_Country"

  def scrapMovieData(link: String): Movie = {
    def getTdByType(doc: Element, textType: String): Option[Element] =
      (doc >> elementList("#infoTable tr td"))
        .dropWhile( _.text != textType )
        .drop(1)
        .headOption

    println(s"$link is started")
    val bodyDoc = JsoupBrowser().get(link).body
    println("\tname parsing...")
    val name = bodyDoc >> text("h1[itemprop=name]")
    println("\torginal name parsing...")
    val originalName = (bodyDoc >?> text("span[itemprop=alternativeHeadline]"))
      .getOrElse(name)
    println("\tslogan parsing...")
    val slogan = getTdByType(bodyDoc, "слоган")
      .map (_.text)
      .flatMap { s => if (s == "-") None else Some(s) }
    println("\tpremiere date parsing...")
    val premDate = (bodyDoc >> attrs("data-date-premier-start-link")("div[data-date-premier-start-link]"))
      .lastOption
      .map{s => s"${s.substring(0,4)}-${s.substring(4,6)}-${s.substring(6)}"}
      .getOrElse("NULL")
    println("\tcountries parsing...")
    val countries = (getTdByType(bodyDoc, "страна").get >> texts("a"))
      .map( c => c.head.toUpper + c.tail )
      .toList
    println("\tgenres parsing...")
    val genres = (bodyDoc >> texts("span[itemprop=genre] a"))
      .map( g => g.head.toUpper + g.tail )
      .toList
    println("\tbudget parsing...")
    val budget = getTdByType(bodyDoc, "бюджет") >> text("div")
    println("\trequired age parsing...")
    val age = (bodyDoc >?> element (".ageLimit"))
      .map (_ attr("class") split(' ') apply(1) replace("age", "") toInt)
    println("\tlength parsing...")
    val length = (bodyDoc >?> text("#runtime"))
      .flatMap { """\d+""".r findFirstIn _ }
      .map { _.toInt }
    println("\tdescription parsing...")
    val description = bodyDoc >?> text("div[itemprop=description]")
    Movie(name, originalName, slogan, premDate, countries, genres, budget, age, length, description)
  }

  def partHandler(ms: List[Movie], number: Int): Unit = {
    val formatMovies = ms.map { case Movie(n, on, s, pD, cs, gs, bdg, age, len, desc) =>
      val fN = wrapString(n)
      val fOn = wrapString(on)
      val fS = s.map(wrapString).getOrElse("NULL")
      val fPD = wrapDate(pD)
      val fCs = cs.map(wrapString)
      val fGs = gs.map(wrapString)
      val fBdg = bdg.map(wrapString).getOrElse("NULL")
      val fAge = age.map(_.toString).getOrElse("NULL")
      val fLen = len.map(_.toString).getOrElse("NULL")
      val fDesc = desc.map(wrapString).getOrElse("NULL")
      (fN, fOn, fS, fPD, fCs, fGs, fBdg, fAge, fLen, fDesc)
    }
    val movieScript = getInsertHeader(movieTable, movieCols) +
      formatMovies.map { case (n, on, s, pD, cs, gs, bdg, age, len, desc) =>
        s"($n, $on, $s, $pD, $bdg, $age, $len, $desc)"
      }.mkString(",\n")
    writeToFile(movieScript, s"$pathToWrite${movieTable}_$number.sql")


    val genreScript = (for { m <- formatMovies; g <- m._6 }
      yield s"""${getInsertHeaderViaSelect(genreTable)} m.ID, g.ID FROM Movie m, Genre g
               |  WHERE m.name = ${m._1} and m.OriginalName = ${m._2} and m.PremiereDate = ${m._4} and g.Name = $g
             """.stripMargin).mkString("\n")
    writeToFile(genreScript, s"$pathToWrite${genreTable}_$number.sql")
    val countryScript = (for { m <- formatMovies; c <- m._5 }
      yield s"""${getInsertHeaderViaSelect(countryTable)} m.ID, c.ID FROM Movie m, Country c
               |  WHERE m.name = ${m._1} and m.OriginalName = ${m._2} and m.PremiereDate = ${m._4} and c.Name = $c
             """.stripMargin).mkString("\n")
    writeToFile(countryScript, s"$pathToWrite${countryTable}_$number.sql")
    println(s"Partition #$number has been done.")
  }


  val movieLinks = readFromFile(pathToLinks)
  iterativelyScrape(scrapMovieData, partHandler, movieLinks, partitionSize)(startFrom)

}
