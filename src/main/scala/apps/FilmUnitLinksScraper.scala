package apps


import utils.Utils._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.browser.JsoupBrowser

import scala.annotation.tailrec

/**
  * Created by HesowcharovU on 22.12.2016.
  */
object FilmUnitLinksScraper extends App {
  import collection.immutable.HashSet

  def toCastUrl(url: String): String = {
    val modificator = if (url.last == '/') {
      "cast/"
    } else {
      "/cast/"
    }
    url + modificator
  }

  @tailrec
  def collectActorLinks(castLinks: List[String], actorLinks: HashSet[String]): List[String] = {
    castLinks match {
      case castLink :: tail =>
        val body = JsoupBrowser().get(castLink).body
        val newActorLinks = (body >> elementList(".dub")).map ( _ >> attr("href")(".info .name a"))
        collectActorLinks(tail, actorLinks ++ newActorLinks)
      case _ => actorLinks.toList
    }
  }

  val castingLinks = readFromFile("D:/sql_scripts/links/hot_movies.txt").map(toCastUrl)
  val actorLinks = collectActorLinks(castingLinks, HashSet())
    .map { s: String => "https://www.kinopoisk.ru" + s }
    .mkString("\n")
  import java.io._
  val pw = new PrintWriter(new File("D:/sql_scripts/links/FilmUnits.txt"))
  pw.write(actorLinks)
  pw.close
}
