package apps

import net.ruippeixotog.scalascraper.scraper.ContentExtractors.{attr, elementList, text}
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL._

import scala.annotation.tailrec

/**
  * Created by HesowcharovU on 22.12.2016.
  */
object MovieLinksScraper extends App{
  def urlByPage(page: Int) =
    "https://www.kinopoisk.ru/s/type/film/list/1/order/rating/m_act[year]/2016/m_act[type]/film/page/" + page

  @tailrec
  def collectLinks(page: Int, acc: List[String]): List[String] = {
    val doc = JsoupBrowser().get(urlByPage(page)).body
    val searchResults = doc >> text("span.search_results_topText")
    val counter = searchResults.filter(_.isDigit).toInt
    if (counter == 0) {
      acc
    } else {
      val links = doc >> elementList(".element").map( _ >> attr("data-url")(".info .name a") )
      collectLinks(page + 1, acc ::: links)
    }
  }

  val movieLinks = collectLinks(1, List())
    .map { "https://www.kinopoisk.ru" + _ }
    .mkString("\n")
  import java.io._
  val pw = new PrintWriter(new File("D:/sql_scripts/links/movie2016.txt"))
  pw.write(movieLinks)
  pw.close
}
