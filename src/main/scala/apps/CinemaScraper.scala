package apps

import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.scraper.ContentExtractors.text
import utils.Utils._

/**
  * Created by HesowcharovU on 22.12.2016.
  */
object CinemaScraper extends App {
  val table = "Cinema"
  val cols = List("Name", "Address", "BookingPhone")
  val doc = JsoupBrowser().get("https://www.kinopoisk.ru/cinemas/tc/431/")
  val cinemaRows = doc.body >> elementList("tr[itemtype=\"http://schema.org/Organization\"]")
  val cinemaData = cinemaRows.map { r =>
    val name = r >> text("a[itemprop=name]")
    val address = r >> text("span[itemprop=streetAddress]")
    val phoneParts = r >> element("tr[itemprop=telephone]")
    val ccode = phoneParts >> text("td.ccode")
    val code = phoneParts >> text("td.code")
    val number = phoneParts >> text("td.numb")
    s"(${wrapString(name)}, ${wrapString(address)}, ${wrapString(s"$ccode-$code-$number")})"
  }
  val script = s"${getInsertHeader(table, cols)} ${cinemaData.mkString(",\n")}"
  writeToFile(script, table)
}
