package apps

/**
  * Created by HesowcharovU on 25.12.2016.
  */
trait ScrapStarter[D] { this: ScraperOf[D] =>

  def links: List[String]

  def partitionSize: Int

  def startPartition: Int

  private def iterativelyScrape(scraper: (String) => D,
                              partHandler: (List[D], Int) => Unit,
                              links: List[String],
                              partitionSize: Int)(startFrom: Int = 1): Unit = {

    def partScrape(part: List[String]): List[D] = part map scraper

    def scrape(parts: List[List[String]], startFrom: Int): Unit = {
      parts
        .zipWithIndex
        .map { case (e, i) => (e, i+1) }
        .drop(startFrom - 1)
        .foreach { case (ls, i) =>
          val partResult = partScrape(ls)
          partHandler(partResult, i)
        }
    }

    val partedLinks = links.grouped(partitionSize).toList
    scrape(partedLinks, startFrom)
  }

  def start(): Unit = {
    iterativelyScrape(elementScrap, partitionHandler, links, partitionSize)(startPartition)
  }

}
