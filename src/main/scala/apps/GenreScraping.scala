package apps

import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.scraper.ContentExtractors.texts
import net.ruippeixotog.scalascraper.dsl.DSL._
import utils.Utils._

/**
  * Created by HesowcharovU on 22.12.2016.
  */
object GenreScraping extends App {
  val table = "Genre"
  val doc = JsoupBrowser().get("https://www.kinopoisk.ru/s/")
  val optionElems = doc.body >> texts("select[id=m_act[genre]] option")
  val genres = optionElems.drop(1)
  val script = genres
    .map { s =>  s.head.toUpper + s.tail }
    .map { s => s"(N'$s')" }
    .mkString(getInsertHeader(table), ", \n" , "")
  writeToFile(script, table)
}
