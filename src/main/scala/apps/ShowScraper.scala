package apps

import net.ruippeixotog.scalascraper.scraper.ContentExtractors.{attr, elementList, text, texts}
import utils.Utils._
import utils.IntervalIterators._
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL._

/**
  * Created by HesowcharovU on 22.12.2016.
  */
object ShowScraper extends App {
  import data.Show
  import com.github.nscala_time.time.Imports._

  val table = "Show"
  val cols = List("CinemaID", "MovieID",  "Date", "Positions", "Price")
  val pathToLinks = args(0)
  val partitionSize = args(1) toInt
  val startFrom = args(2) toInt
  val leftDate = args(3).toDateTime
  val rightDate = args(4).toDateTime
  val pathToWrite = args(5)
  val movieLinks = readFromFile(pathToLinks)

  val cityAfishaSuffix = "afisha/tc/431/"
  val fixLeftDate = if (leftDate.isAfterNow) leftDate else DateTime.now
  val dateInterval = fixLeftDate to (rightDate + 1.day) iterateBy (1 day) toList
  val formattedDates = dateInterval.map(convertToString)
  val browser = JsoupBrowser()

  def scrap(link: String): List[Show] = {
    println(s"Show Scraper: $link is started")
    val movie = MovieScraper.scrapMovieData(link)
    val shows = formattedDates.flatMap { date =>
      println("\tdate is " + date)
      val dateSuffix = "day_view/" + date
      val showLink = s"$link${cityAfishaSuffix}day_view/$dateSuffix"
      val bodyDoc = browser.get(showLink).body
      println("\ttoday is parsing...")
      val optDateInfo = bodyDoc >?> text("div.showDate")
      val today = optDateInfo
        .map(_.replace("Сегодня,", ""))
        .flatMap { date =>
          """\s*(\W+),""".r
          .findFirstMatchIn(date)
          .map(_.group(1))
      }.getOrElse("None")
      val optRows = bodyDoc >?> elementList("tr.cinema_row")
      val dayShows = optRows.map {
        _.flatMap { row =>
          println("\tcinema is parsing...")
          val cinema = row >> text("td.col_one span.name a")
          println("\ttimes are parsing...")
          val times = row >> texts("td.col_sec div.time a")
          println("\tmax position is randoming")
          val positions = getRandomPositionsBetween(5, 12)
          println("\tprice is calculating")
          val price = baseCinemaPrices(cinema) + dayFees(today)
          times
            .map { t => Show(cinema, movie, s"$date $t", positions, price) }
            .toList
        }
      }.getOrElse{ println("shows isn't on this date"); List() }
      dayShows
    }
    shows
  }

  def handler(shows: List[List[Show]], partition: Int): Unit = {
    if (shows.length != 0 && shows.head.length != 0) {
      val header = getInsertHeader(table, cols)
      val script = "set dateformat ymd\n" + (for {ss <- shows; s <- ss} yield {
        s"""$header(
           |  (${selectScalarValues("Cinema", "ID")("Name" -> s.cinema)}),
           |  (${selectScalarValues("Movie", "ID")("Name" -> s.movie.name, "OriginalName" -> s.movie.originalName)}),
           |  ${wrapDate(s.date)},
           |  ${s.positions},
           |  ${s.price}
           |)
       """.stripMargin
      }).mkString("\n")
      writeToFile(script, s"$pathToWrite${table}_$partition.sql")
    } else {
      println("Nothing to write")
    }
    println(s"Partition #$partition has been done.")

  }

  iterativelyScrape(scrap, handler, movieLinks, partitionSize)(startFrom)
}
