package apps

/**
  * Created by HesowcharovU on 25.12.2016.
  */
trait ScraperOf[D] {
  def elementScrap(link: String): D

  def partitionHandler(partition: List[D], number: Int): Unit
}
