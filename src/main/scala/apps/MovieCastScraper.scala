package apps

import net.ruippeixotog.scalascraper.scraper.ContentExtractors.{attrs, texts, text}
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL._
import utils.Utils._

/**
  * Created by HesowcharovU on 22.12.2016.
  */
object MovieCastScraper extends App {

  import data.Movie
  case class PartialFilmUnit(firstName: String, lastName: Option[String])
  case class Role(name: String)

  val neededRoles = List("director",
    "actor",
    "producer",
    "writer",
    "operator",
    "composer",
    "design",
    "editor"
  )
  val keyToColumnRole = Map("director" -> "Режиссер",
    "actor" -> "Актер",
    "producer" -> "Продюсер",
    "writer" -> "Сценарист",
    "operator" -> "Оператор",
    "composer" -> "Композитор",
    "design" -> "Художник",
    "editor" -> "Монтажер"
  )

  val table = "Movie_FilmUnit_FilmRole"

  val pathToLinks = args(0)
  val partitionSize = args(1) toInt
  val startFrom = args(2) toInt
  val pathToWrite = args(3)
  val movieLinks = readFromFile(pathToLinks)

  def scrap(movieLink: String): (Movie, Map[String, List[PartialFilmUnit]]) = {

    val movie = MovieScraper.scrapMovieData(movieLink)
    val bodyDoc = JsoupBrowser().get(movieLink + "cast/").body
    println("\troles parsing...")
    val roles = bodyDoc >> attrs("name")("a[name]")
    val rangeRoles = roles zipAll (roles.drop(1), "-", "-")
    println("\tactors parsing...")
    val cast: Map[String, List[PartialFilmUnit]] = roles.map { r =>
      val filmUnits = (bodyDoc >> texts(s"a[name=$r] ~ div .actorInfo .name a"))
        .map { n =>
          val nameParts = n.split(' ')
          val firstName = nameParts(0)
          val lastName = if (nameParts.size == 1) {
            None
          } else {
            Some(nameParts.drop(1).mkString(" "))
          }
          PartialFilmUnit(firstName, lastName)
        }
      (r, filmUnits.toList)
    }.toMap
    val roleToUniqueFilmUnits = cast.map { case (r, names) =>
      val r1 = rangeRoles.find(_._1 == r).get._2
      if(cast.contains(r1)) {
        (r, names diff cast(r1))
      } else {
        (r, names)
      }
    }.filter { case(r, _) => neededRoles.contains(r) }
      .map { case (r, names) => (keyToColumnRole(r), names) }
    (movie, roleToUniqueFilmUnits)
  }

  def partHandler(ds: List[(Movie, Map[String, List[PartialFilmUnit]])], number: Int): Unit = {
    val scripts = (for {data <- ds} yield {
      val (m, roleToActors) = data
      val script = (for {(r, as) <- roleToActors; a <- as} yield
        s"""${getInsertHeaderViaSelect(table)} m.ID, fu.ID, fr.ID FROM Movie m, FilmUnit fu, FilmRole fr
           |  WHERE m.Name = ${wrapString(m.name)} and m.OriginalName = ${wrapString(m.originalName)}
           |  and m.PremiereDate = ${wrapDate(m.premiereDate)} and fu.FirstName = ${wrapString(a.firstName)}
           |  and fu.LastName = ${a.lastName.map(wrapString).getOrElse("NULL")} and fr.Name = ${wrapString(r)}
         """.stripMargin).mkString("\n")
      script
    }).mkString("\n\n")
    writeToFile(scripts, s"$pathToWrite${table}_$number.sql")
    println(s"Partition #$number has been done.")
  }

  iterativelyScrape(scrap, partHandler, movieLinks, partitionSize)(startFrom)
}
