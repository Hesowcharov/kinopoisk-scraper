package apps

import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.scraper.ContentExtractors.{attr, elementList, text, texts}
import net.ruippeixotog.scalascraper.dsl.DSL._
import utils.Utils._

/**
  * Created by HesowcharovU on 22.12.2016.
  */
object NominationScraper extends App {
  val table = "Nomination"
  val columns = List("FilmAwardID", "Name")
  val partitionSize = args(0) toInt
  val startFrom = args(1) toInt
  val pathToWrite = args(2)

  val browser = JsoupBrowser()
  val domain = "https://www.kinopoisk.ru"
  val doc = browser.get(s"$domain/awards/")
  val awardLinks = (doc.body >> elementList("td.news"))
    .map(_ >?> attr("href")("a"))
    .map( _.map ( part => s"$domain$part") )
    .filter(_.isDefined)
    .map (_.get)

  val lastFullYear = 2014
  val nominationsLinks = awardLinks.map(_ + s"$lastFullYear/")

  def scrap(link: String): (String, List[String]) = {
    println(s"$link is started...")
    val bodyDoc = browser.get(link).body
    println("\taward name parsing")
    val awardName = bodyDoc >> text("a.all")
    val nominations = (bodyDoc >> texts("#itemList td.news b")).toList
    (awardName, nominations)
  }

  def handler(data: List[(String, List[String])], number: Int): Unit = {
    val script = (for { (a, ns) <- data; n <- ns } yield
      s"""${getInsertHeader(table, columns)}(
         |  (SELECT TOP 1 ID FROM FilmAward WHERE Name = ${wrapString(a)}), ${wrapString(n)}
         |)
       """.stripMargin).mkString("\n")
    writeToFile(script, s"$pathToWrite${table}_$number.sql")
    println(s"Partition #$number has been done.")
  }

  iterativelyScrape(scrap, handler, nominationsLinks, 1)(startFrom)
}
