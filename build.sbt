name := "kinopoisk-scraper"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "net.ruippeixotog" %% "scala-scraper" % "1.1.0",
  "com.github.nscala-time" %% "nscala-time" % "2.14.0"
)

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

//mainClass in (Compile, run) := Some("ShowScraper")